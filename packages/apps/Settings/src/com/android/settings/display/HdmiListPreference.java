package com.android.settings.display;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.os.SystemProperties;
import android.preference.ListPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


import android.os.DisplayOutputManager;
import android.os.SystemProperties;

import com.android.settings.data.ConstData;

import java.util.ArrayList;

public class HdmiListPreference extends ListPreference {

		private static final String TAG = "DeviceListPreference";
	    /**
	     * 分辨率设置
	     */
	    protected ListPreference mResolutionPreference;
	    /**
	     * 当前显示设备对应的信息
	     */
	    protected DisplayInfo mDisplayInfo;

	    /**
	     * 标识平台
	     */
	    protected String mStrPlatform;
	    /**
	     * 显示管理
	     */
	    protected DisplayManager mDisplayManager;
	    /**
	    * 原来的分辨率
	    */
	    private String mOldResolution;

	    public HdmiListPreference(Context context, AttributeSet attrs) {
	        super(context, attrs);
	        initData();
	        updateResolutionValue();
	    }
	    protected void initData(){
	        mStrPlatform = SystemProperties.get("ro.board.platform");
	        mDisplayManager = (DisplayManager)getContext().getSystemService(Context.DISPLAY_SERVICE);
	        
	        if (mStrPlatform.contains("3399")) {
	            mDisplayInfo = getDisplayInfo();
	        }
	        mResolutionPreference = this;
	        
	        setEntries(mDisplayInfo.getModes());
	        setEntryValues(mDisplayInfo.getModes());

	    }
	    
	    /**
	     * 还原分辨率值
	     */
	    public void updateResolutionValue(){
	        String resolutionValue = null;
	        if(mStrPlatform.contains("3399")){
	            resolutionValue = DrmDisplaySetting.getCurDisplayMode(mDisplayInfo);
	            Log.i(TAG, "3399 resolutionValue:" + resolutionValue);
	            if(resolutionValue != null)
	                mResolutionPreference.setValue(resolutionValue);
	        }else{
	            DisplayOutputManager displayOutputManager = null;
	            try{
	                displayOutputManager = new DisplayOutputManager();
	                resolutionValue = displayOutputManager.getCurrentMode(mDisplayInfo.getDisplayId() == 0 ? 0 : 1, mDisplayInfo.getType());
	            }catch (Exception e){
	                Log.i(TAG, "updateResolutionValue->exception:" + e);
	            }
	            if(resolutionValue != null)
	                mResolutionPreference.setValue(resolutionValue);
	            if(mOldResolution == null)
	                mOldResolution = resolutionValue;
	        }
	    }
	 
	    private DisplayInfo getDisplayInfo() {
	        return DrmDisplaySetting.getHdmiDisplayInfo();
	    }

}
